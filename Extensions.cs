﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler {
    internal static class Extensions {
        public static bool IsPalindrome(this string str) {
            int len = str.Length;
            for (int i = 0; i < (len / 2) + 1; ++i) {
                if (str[i] != str[len - i - 1]) return false;
            }

            return true;
        }

        public static long Prod(this IEnumerable<long> numbers) {
            long result = 1;
            foreach (long number in numbers) result *= number;
            return result;
        }

        public static long Prod(this IEnumerable<int> numbers) {
            long result = 1;
            foreach (int number in numbers) result *= number;
            return result;
        }
    }
}
