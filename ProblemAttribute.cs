﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler {

    class ProblemAttribute : Attribute {

        private string _description;

        public const string DefaultDescription = "This Problem has no description.";

        public ProblemAttribute() {
            Description = DefaultDescription;
        }

        public string Description {
            get {
                if (!IsImplemented) {
                    return "NOT IMPLEMENTED YET.";
                }
                else return _description;
            }
            set { _description = value; }
        }

        public int Number { get; set; } = 0;
        public bool IsImplemented { get; set; } = true;
        public bool Ignore { get; set; } = false;
    }
}
