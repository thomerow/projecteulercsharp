﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler {

    static class Problems {

        #region "  Reflection Stuff  "

        public static void SolveAll() {
            Type tProblems = typeof(Problems);
            var methods
                = tProblems
                    .GetMethods(BindingFlags.Static | BindingFlags.Public)
                    .Where(m => (m.GetCustomAttributes(typeof(ProblemAttribute), false).Length > 0)
                                    && (m.GetParameters().Length == 0)
                                    && (m.ReturnType == typeof(void)));

            foreach (MethodInfo mi in methods) {
                var attr = ((ProblemAttribute) mi.GetCustomAttribute(typeof(ProblemAttribute)));
                if (attr.Ignore) {
                    Console.WriteLine("No. {0}: IGNORED.", attr.Number);
                    continue;
                }
                Console.WriteLine("No. {0}: {1}", attr.Number, attr.Description);
                var sw = Stopwatch.StartNew();
                mi.Invoke(null, null);
                sw.Stop();
                Console.WriteLine(string.Format("({0}ms)", sw.ElapsedMilliseconds));
                Console.WriteLine();
            }
        }

        #endregion // Reflection Stuff


        #region "  Problems  "

        [Problem(Number = 1, Description = "Find the sum of all the multiples of 3 or 5 below 1000.")]
        public static void Problem0001() {
            int result
                = Enumerable.Range(1, 999)
                    .Where(n => ((n % 3) == 0) || ((n % 5) == 0))
                    .Sum();
            Console.WriteLine(result);
        }

        [Problem(Number = 2, Description = "By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.")]
        public static void Problem0002() {
            long result
                = Fibonacci()
                    .TakeWhile(n => n <= 4000000)
                    .Where(n => ((n % 2) == 0))
                    .Sum();
            Console.WriteLine(result);
        }

        [Problem(Number = 3, Description = "What is the largest prime factor of the number 600851475143?")]
        public static void Problem0003() {
            const long TheNumber = 600851475143;
            var factors = Factorize(TheNumber);
            Console.WriteLine("Prime factors: {0}. Result: {1}", string.Join(", ", factors), factors.Last());
        }

        struct Product {
            public int Fact1;
            public int Fact2;
            public int Number;
        }

        [Problem(Number = 4, Description = "Find the largest palindrome made from the product of two 3-digit numbers.")]
        public static void Problem0004() {
            var result = new Product();
            for (int i = 999; i >= 100; --i) {
                for (int j = i; j >= 100; --j) {
                    int candidate = i * j;
                    if (candidate.ToString().IsPalindrome()) {
                        if (candidate > result.Number) {
                            result.Fact1 = i; result.Fact2 = j; result.Number = candidate;
                        }
                        break;
                    }
                }
            }

            Console.WriteLine("Numbers: {0}, {1}. Result: {2}", result.Fact1, result.Fact2, result.Number);
        }

        [Problem(Number = 4, Description = "What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?")]
        public static void Problem0005() {
            int result = 0, candidate = 20;
            var relevant = new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            for (;;) {
                bool bFound = true;
                for (int j = 0; j < relevant.Length; ++j) {
                    if ((candidate % relevant[j]) > 0) { bFound = false; break; }
                }
                if (bFound) { result = candidate; break; }
                candidate += 20;
            }

            Console.WriteLine(result);
        }

        [Problem(Number = 5, Description = "Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.")]
        public static void Problem0006() {
            // Calculate the sum of the first 100 numbers with the gaußsche Summenformel
            // (n² + n) / 2
            int n = 100;
            long squareOfSum = (long) Math.Pow(((long) Math.Pow(n, 2) + n) / 2, 2);
            // Sum of the first n square numbers:
            // (n(n + 1)(2n + 1)) / 6
            long sumOfSquares = (n * (n + 1) * (2 * n + 1)) / 6;
            long result = squareOfSum - sumOfSquares;
            Console.WriteLine(result);
        }

        [Problem(Number = 7, Description = "What is the 10 001st prime number?")]
        public static void Problem0007() {
            Console.WriteLine(Primes().ElementAt(10000));
        }

        [Problem(Number = 8, Description = "Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?")]
        public static void Problem0008() {
            string number = "73167176531330624919225119674426574742355349194934"
                + "96983520312774506326239578318016984801869478851843"
                + "85861560789112949495459501737958331952853208805511"
                + "12540698747158523863050715693290963295227443043557"
                + "66896648950445244523161731856403098711121722383113"
                + "62229893423380308135336276614282806444486645238749"
                + "30358907296290491560440772390713810515859307960866"
                + "70172427121883998797908792274921901699720888093776"
                + "65727333001053367881220235421809751254540594752243"
                + "52584907711670556013604839586446706324415722155397"
                + "53697817977846174064955149290862569321978468622482"
                + "83972241375657056057490261407972968652414535100474"
                + "82166370484403199890008895243450658541227588666881"
                + "16427171479924442928230863465674813919123162824586"
                + "17866458359124566529476545682848912883142607690042"
                + "24219022671055626321111109370544217506941658960408"
                + "07198403850962455444362981230987879927244284909188"
                + "84580156166097919133875499200524063689912560717606"
                + "05886116467109405077541002256983155200055935729725"
                + "71636269561882670428252483600823257530420752963450";
            long largest = 0;
            for (int i = 0; i < number.Length - 13; ++i) {
                string substring = number.Substring(i, 13);
                long sum = substring.Select(c => int.Parse(c.ToString())).Prod();
                largest = Math.Max(sum, largest);
            }
            Console.WriteLine(largest);
        }

        [Problem(Number = 9, Description = "There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.")]
        public static void Problem0009() {
            for (int c = 3; c <= 997; ++c) {    // if a < b < c and a is >= 1, then b is at least 2, so c can be at most 1000 - 1 - 2
                int bMax = Math.Min(c - 1, 999 - c);    // b has to fit into 1000 minus c minus the min. value of a (1)
                for (int b = (1000 - c) / 2 + 1; b < bMax; ++b) {   // because b must be greater than a it has to be at least as big as (1000 - c) / 2 + 1
                    int a = 1000 - b - c;
                    if (a * a + b * b == c * c) {
                        Console.WriteLine("a = {0}, b = {1}, c = {2}, abc = {3}", a, b, c, a * b * c);
                        return;
                    }
                }
            }
        }

        [Problem(Number = 10, Ignore = true, Description = "Find the sum of all the primes below two million.")]
        public static void Problem0010() {
            long sum = 0;
            foreach (long prime in Primes()) {
                if (prime >= 2000000) break;
                sum += prime;
            }
            Console.WriteLine(sum);
        }

        #endregion // Problems


        #region " Helpers "

        static IEnumerable<long> Fibonacci() {
            long current = 1;
            long previous = 0;
            for (;;) {
                long result = current + previous;
                previous = current;
                current = result;
                yield return result;
            }
        }

        /// <summary>
        /// Generates prime numbers.
        /// </summary>
        static IEnumerable<long> Primes() {
            yield return 2;
            var found = new List<long>(new long[] { 2 });
            // Count upwards from 5 leaving out even numbers
            for (int i = 3; ; i += 2) {
                // If the current number can be divided without remainder 
                // by any of the prime numbers found so far, it is not prime
                if (found.Any(p => (i % p) == 0)) continue;
                // Current number is prime. Store and return it.
                found.Add(i);
                yield return i;
            }
        }

        /// <summary>
        /// Generates large prime numbers.
        /// </summary>
        static IEnumerable<BigInteger> BigPrimes() {
            yield return new BigInteger(2);
            var found = new List<BigInteger>(new BigInteger[] { 2 });
            // Count upwards from 5 leaving out even numbers
            for (var i = new BigInteger(3); ; i += 2) {
                // If the current number can be divided without remainder 
                // by any of the prime numbers found so far, it is not prime
                if (found.Any(p => (i % p) == 0)) continue;
                // Current number is prime. Store and return it.
                found.Add(i);
                yield return i;
            }
        }

        /// <summary>
        /// Finds prime factors of a number and returns them in ascending order.
        /// </summary>
        private static List<long> Factorize(long n) {
            var result = new List<long>();
            foreach (long p in Primes()) {
                while ((n % p) == 0) {
                    result.Add(p);
                    n /= p;
                    if (n == 1) break;
                }
                if (n == 1) break;
            }
            return result;
        }

        /// <summary>
        /// Finds prime factors of a large number and returns them 
        /// in ascending order. (Unusably slow)
        /// </summary>
        private static List<BigInteger> Factorize(BigInteger n) {
            var result = new List<BigInteger>();
            foreach (BigInteger p in BigPrimes()) {
                while ((n % p) == 0) {
                    result.Add(p);
                    n /= p;
                    if (n == 1) break;
                }
                if (n == 1) break;
            }
            return result;
        }

        #endregion // Helpers
    }
}
